package com.example.testbottomnavigationbar

import android.os.Bundle
import android.util.SparseArray
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.NavGraph
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.navOptions
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView


class MainActivity : AppCompatActivity() {
    private lateinit var homeFragment: HomeFragment
    private lateinit var navHostFragment: NavHostFragment
    private lateinit var navController: NavController
    private var currentTabIndex = 0
    private val savedBackStackStates = SparseArray<Bundle>()


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("currentTabIndex", currentTabIndex)

        for (i in 0 until savedBackStackStates.size()) {
            val tabId = savedBackStackStates.keyAt(i)
            val backStackState = savedBackStackStates.valueAt(i)
            outState.putBundle("backStackState_$tabId", backStackState)
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        currentTabIndex = savedInstanceState.getInt("currentTabIndex")

        for (i in 0 until savedBackStackStates.size()) {
            val tabId = savedBackStackStates.keyAt(i)
            val backStackState = savedInstanceState.getBundle("backStackState_$tabId")
            savedBackStackStates.put(tabId, backStackState)
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navHostFragment =
            supportFragmentManager.findFragmentById(R.id.open_fragment_container) as NavHostFragment
        homeFragment =
            navHostFragment.childFragmentManager.fragments.first { it is HomeFragment } as HomeFragment
        navController = navHostFragment.navController
        val navGraph: NavGraph = navController.navInflater.inflate(R.navigation.nav_graph)
        navController.graph = navGraph
        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottom_navigation)
        bottomNavigationView.setupWithNavController(navController)

        bottomNavigationView.setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.home -> navigateTo(R.id.homeFragment)
                R.id.chat -> navigateTo(R.id.chatFragment)
                R.id.cart -> navigateTo(R.id.cartFragment)
                R.id.more -> navigateTo(R.id.moreFragment)
            }
            true
        }
    }

    private fun navigateTo(destinationId: Int) {
        val shouldSaveAndRestoreState = navController.currentDestination?.id != destinationId
        navController.navigate(destinationId, null, navOptions {
            launchSingleTop = true
            restoreState = shouldSaveAndRestoreState
            popUpTo(navController.graph.findStartDestination().id) {
                saveState = shouldSaveAndRestoreState
            }
        })
    }
}
