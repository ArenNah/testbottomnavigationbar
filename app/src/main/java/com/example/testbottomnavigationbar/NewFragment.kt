package com.example.testbottomnavigationbar

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import com.example.testbottomnavigationbar.databinding.NewFragmentBinding


class NewFragment : Fragment(R.layout.new_fragment) {
    private lateinit var binding: NewFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = NewFragmentBinding.inflate(layoutInflater)

        binding.apply {

            newFragmentButton1.setOnClickListener {
                navigateTo(R.id.newFragment1)
            }
        }
        return binding.root
    }

    private fun navigateTo(destinationId: Int) {
        val shouldSaveAndRestoreState = findNavController().currentDestination?.id != destinationId
        findNavController().navigate(destinationId, null, navOptions {
            launchSingleTop = true
            restoreState = shouldSaveAndRestoreState
            popUpTo(R.id.newFragment) {
                saveState = shouldSaveAndRestoreState
            }
        })
    }
}

