package com.example.testbottomnavigationbar

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import com.example.testbottomnavigationbar.databinding.HomeFragmentBinding


class HomeFragment : Fragment() {
    private lateinit var binding: HomeFragmentBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = HomeFragmentBinding.inflate(layoutInflater)

        binding.apply {
            newFragmentButton.setOnClickListener {
                navigateTo(R.id.nav_grapgh_home)
            }
            openCartFragmentButton.setOnClickListener {
                navigateTo(R.id.cartFragment)
            }
            openChatFragmentButton.setOnClickListener {
                navigateTo(R.id.chatFragment)
            }
            openMoreFragmentButton.setOnClickListener {
                navigateTo(R.id.moreFragment)
            }
        }
        return binding.root
    }

    private fun navigateTo(destinationId: Int) {
        val shouldSaveAndRestoreState = findNavController().currentDestination?.id != destinationId
        findNavController().navigate(destinationId, null, navOptions {
            launchSingleTop = true
            restoreState = shouldSaveAndRestoreState
            popUpTo(R.id.nav_grapgh_home) {
                saveState = shouldSaveAndRestoreState
            }
        })
    }
}

